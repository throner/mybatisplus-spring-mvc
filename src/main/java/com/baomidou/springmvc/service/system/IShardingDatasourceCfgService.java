package com.baomidou.springmvc.service.system;

import com.sharding.model.ShardingDatasourceCfg;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 数据库连接路由表 服务类
 * </p>
 *
 * @author chenjs
 * @since 2017-08-05
 */
public interface IShardingDatasourceCfgService extends IService<ShardingDatasourceCfg> {
    /**
     * 获取数据源名
     * @return 获取数据源名列表
     */
    public List<String> getDataSourceNameList();
}
