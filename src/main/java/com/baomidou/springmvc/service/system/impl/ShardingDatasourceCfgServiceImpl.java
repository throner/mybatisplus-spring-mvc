package com.baomidou.springmvc.service.system.impl;

import com.sharding.model.ShardingDatasourceCfg;
import com.sharding.mapper.ShardingDatasourceCfgMapper;
import com.baomidou.springmvc.service.system.IShardingDatasourceCfgService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 数据库连接路由表 服务实现类
 * </p>
 *
 * @author chenjs
 * @since 2017-08-05
 */
@Service
public class ShardingDatasourceCfgServiceImpl extends ServiceImpl<ShardingDatasourceCfgMapper, ShardingDatasourceCfg> implements IShardingDatasourceCfgService {
    private static Logger log = Logger.getLogger(ShardingDatasourceCfgServiceImpl.class);
    /**
     * 获取数据源名
     *
     * @return 获取数据源名列表
     */
    public List<String> getDataSourceNameList() {
        return baseMapper.getDataSourceNameList();
    }
}
