package com.baomidou.springmvc.service.system.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import  com.baomidou.springmvc.model.system.SysUser1;
import com.baomidou.springmvc.mapper.system.SysUser1Mapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.springmvc.service.system.ISysUser1Service;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 系统用户服务实现
 * </p>
 *
 * @author chenjs
 * @since 2017-07-26
 */
@Service
public class SysUser1ServiceImpl extends ServiceImpl<SysUser1Mapper, SysUser1> implements ISysUser1Service {
    private static Logger log = Logger.getLogger(ShardingDatasourceCfgServiceImpl.class);
    @Resource
    private SysUser1Mapper sysUser1Mapper;
    @Override
    public void test() {
        if(baseMapper==null){
            log.error("33333333333333");
        }else{
            log.error("4444444444");
        }
        sysUser1Mapper.selectObjs(new EntityWrapper<SysUser1>().where("1","2"));
    }
}
