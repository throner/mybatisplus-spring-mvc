package com.baomidou.springmvc.mapper.system;

import com.baomidou.springmvc.model.system.SysUser1;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  * 系统用户 Mapper 接口
 * </p>
 *
 * @author chenjs
 * @since 2017-07-26
 */
public interface SysUser1Mapper extends BaseMapper<SysUser1> {

}