package com.baomidou.springmvc.mapper.system;

import com.baomidou.springmvc.common.SuperMapper;
import com.baomidou.springmvc.model.system.PrcPojo;
import com.baomidou.springmvc.model.system.User;

import java.util.List;

/**
 *
 * User 表数据库控制层接口
 *
 */
public interface UserMapper extends SuperMapper<User> {

    public List<User> testSql(User user);

    // 调用存储过程默认为void返回
    public void testPrc(PrcPojo prcPojo);

    // 调用函数默认为void返回
    public String testFunc(PrcPojo prcPojo);
}
