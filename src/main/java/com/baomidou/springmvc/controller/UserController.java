package com.baomidou.springmvc.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.springmvc.mapper.system.UserMapper;
import com.baomidou.springmvc.model.system.PrcPojo;
import com.baomidou.springmvc.model.system.SysUser1;
import com.baomidou.springmvc.service.system.ISysUser1Service;
import com.cache.CacheObj;
import com.cache.TestCache;
import com.sharding.ShardingContextHolder;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.springmvc.model.system.User;
import com.baomidou.springmvc.service.system.IUserService;

import javax.annotation.Resource;

/**
 * Author: D.Yang
 * Email: koyangslash@gmail.com
 * Date: 16/10/9
 * Time: 上午11:58
 * Describe: 用户控制器
 * 
 * 代码生成器，参考源码测试用例：
 * 
 * /mybatis-plus/src/test/java/com/baomidou/mybatisplus/test/generator/MysqlGenerator.java
 *
 */
@Controller
public class UserController extends BaseController {

    @Resource
    private final IUserService userService;
    @Resource
    private SqlSessionFactory sqlSessionFactory;

    @Resource
    private ISysUser1Service sysUser1Service;

    @Resource
    private TestCache testCache;

    @Resource
    private UserMapper userMapper;



    @Autowired
    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("index");
        EntityWrapper<User> ew = new EntityWrapper<User>();
//        ew.where("id={0}","784972358981328902")
//                .and("id={0} and ctime>{1}","784972358981328902",new Date());
//        modelAndView.addObject("userList", userService.selectList(ew));
        ShardingContextHolder.setShardingValue("2");
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try{
//            UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            PrcPojo prcPojo = new PrcPojo();
            prcPojo.setA(1);
            prcPojo.setB(5);
            userMapper.testPrc(prcPojo);
//            String c = userMapper.testFunc(prcPojo);
//            System.out.println("C="+c);
            System.out.println("C="+prcPojo.getC());
        }catch(Exception ex){
            sqlSession.rollback();
        }finally {
            //释放资源
            sqlSession.close();
        }
        ShardingContextHolder.setShardingValue("2");
        modelAndView.addObject("userList", userService.selectList(ew));
        testCache.delString("ssss");
        System.out.println("real get:"+testCache.getString("ssss"));
        CacheObj co = new CacheObj();
        co.setKey("mmm");
        co.setValue("kkkk");
//        testCache.delCacheObj(co);

        System.out.println("real obj:"+testCache.getCacheObjList(co).get(0).getValue());
        return modelAndView;
    }


    @RequestMapping("/testXmlQuery")
    public ModelAndView testXmlQuery(ModelAndView modelAndView) {
        modelAndView.setViewName("index_xml");
        EntityWrapper<SysUser1> ew = new EntityWrapper<SysUser1>();
        modelAndView.addObject("userList", sysUser1Service.selectList(ew));
        return modelAndView;
    }

    @RequestMapping("/preSave")
    public ModelAndView preSave(ModelAndView modelAndView, @RequestParam(value = "id", required = false) Long id) {
        modelAndView.setViewName("save");
        if (id != null) {
            modelAndView.addObject("user", userService.selectById(id));
        }
        return modelAndView;
    }

    @RequestMapping("/preSaveXml")
    public ModelAndView preSaveXml(ModelAndView modelAndView, @RequestParam(value = "id", required = false) Long id) {
        modelAndView.setViewName("save_xml");
        if (id != null) {
            modelAndView.addObject("user", userService.selectById(id));
        }
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping("save")
    public Object save(User user) {
        if (user.getId() == null) {
            return userService.insert(user) ? renderSuccess("添加成功") : renderError("添加失败");
        } else {
            return userService.updateById(user) ? renderSuccess("修改成功") : renderError("修改失败");
        }
    }


    @ResponseBody
    @RequestMapping("saveXml")
    public Object saveXml(SysUser1 user) {
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//        if (user.getId() == null) {
//            int iResult = 0;
//            //提交事务
//            try{
//                iResult = sqlSession.insert("insertSql",user);
//                sqlSession.commit();
//            }catch(Exception ex){
//                ex.printStackTrace();
//                sqlSession.rollback();
//            }finally {
//                //释放资源
//                sqlSession.close();
//            }
//
//
//            if(iResult>0){
//                return renderSuccess("添加成功");
//            }else{
//                return renderError("添加失败");
//            }
//
//        } else {
//            return userService.updateById(user) ? renderSuccess("修改成功") : renderError("修改失败");
//        }
        if (user.getId() == null) {
            return sysUser1Service.insert(user) ? renderSuccess("添加成功") : renderError("添加失败");
        } else {
            return sysUser1Service.updateById(user) ? renderSuccess("修改成功") : renderError("修改失败");
        }
    }


    @ResponseBody
    @RequestMapping("/delete")
    public Object delete(@RequestParam(value = "id", required = false) Long id) {
        return userService.deleteById(id) ? renderSuccess("删除成功") : renderError("删除失败");
    }
}
