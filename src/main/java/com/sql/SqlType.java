package com.sql;

public enum SqlType {
	K_SELECT,
	K_UPDATE,
	K_DELETE,
	K_INSERT,
	K_REPLACE,
	K_CREATE,
	K_PRC_CALL
}
