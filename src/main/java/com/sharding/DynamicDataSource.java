package com.sharding;


import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
/**
 * Created by jiangwenping on 17/3/6.
 */

public class DynamicDataSource extends AbstractRoutingDataSource {
    private static Logger log = Logger.getLogger(DynamicDataSource.class);
    public DynamicDataSource(){
        System.out.println("DynamicDataSource create.......");

    }
    @Override
    protected Object determineCurrentLookupKey() {
        log.debug("ShardingContextHolder.getShardingDs():"+ShardingContextHolder.getShardingDs());
        return ShardingContextHolder.getShardingDs();
    }

}