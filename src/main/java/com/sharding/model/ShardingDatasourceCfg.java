package com.sharding.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据库连接路由表
 * </p>
 *
 * @author chenjs
 * @since 2017-08-05
 */
@TableName("sharding_datasource_cfg")
public class ShardingDatasourceCfg extends Model<ShardingDatasourceCfg> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据库对象名
     */
    @TableId("obj_name")
	private String objName;
    /**
     * 数据库对象类型
     */
	@TableField("obj_type")
	private String objType;
    /**
     * 数据库对象分组名
     */
	@TableField("group_name")
	private String groupName;
    /**
     * 分库属性值,如公司ID等，根据业务而定
     */
	@TableField("sharding_value")
	private String shardingValue;
    /**
     * 数据源名
     */
	@TableField("datasource_name")
	private String datasourceName;


	public String getObjName() {
		return objName;
	}

	public void setObjName(String objName) {
		this.objName = objName;
	}

	public String getObjType() {
		return objType;
	}

	public void setObjType(String objType) {
		this.objType = objType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getShardingValue() {
		return shardingValue;
	}

	public void setShardingValue(String shardingValue) {
		this.shardingValue = shardingValue;
	}

	public String getDatasourceName() {
		return datasourceName;
	}

	public void setDatasourceName(String datasourceName) {
		this.datasourceName = datasourceName;
	}

	@Override
	protected Serializable pkVal() {
		return this.objName;
	}

	@Override
	public String toString() {
		return "ShardingDatasourceCfg{" +
			"objName=" + objName +
			", objType=" + objType +
			", groupName=" + groupName +
			", shardingValue=" + shardingValue +
			", datasourceName=" + datasourceName +
			"}";
	}
}
