package com.sharding;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/** 分库线程保持类
 * Created by chenjiansheng on 17/3/6.
 */
@Service
public class ShardingContextHolder {
    // 用ThreadLocal来设置当前线程使用哪个dataSource
    private static final ThreadLocal<String> shardingDsContextHolder = new ThreadLocal<String>();
    // 用ThreadLocal来设置当前线程使用的分库值
    private static final ThreadLocal<String> shardingValueContextHolder = new ThreadLocal<String>();

    public static void setShardingDs(String shardingType) {
        shardingDsContextHolder.set(shardingType);
    }
    public static String getShardingDs() {
        return shardingDsContextHolder.get();
    }

    public static void setShardingValue(String shardingValue) {
        shardingValueContextHolder.set(shardingValue);
    }
    public static String getShardingValue() {
        return shardingValueContextHolder.get();
    }

    public static void clearShardingDs() {
        shardingDsContextHolder.remove();
    }

    /**
     * 清除分库参考值，在不是使用分库参考值时，需要清除。但是清除后如果逻辑上需要再设置原来的参考值，则再设置。由人为程序控制
     */
    public static void clearShardingValue() {
        shardingValueContextHolder.remove();
    }

}
