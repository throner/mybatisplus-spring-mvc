package com.sharding.mapper;

import com.baomidou.springmvc.common.SuperMapper;
import com.sharding.model.ShardingDatasourceCfg;
import java.util.List;

/**
 * <p>
  * 数据库连接路由表 Mapper 接口
 * </p>
 *
 * @author chenjs
 * @since 2017-08-05
 */
public interface ShardingDatasourceCfgMapper extends SuperMapper<ShardingDatasourceCfg> {
    /**
     * 获取数据源名
     * @return 获取数据源名列表
     */
    public List<String> getDataSourceNameList();
}