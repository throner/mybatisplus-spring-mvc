/** 
* 2015-11-21 
* User.java 
* author:Zack Chan
*/ 
package com.swagger.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "The User of the APP")
public class User {
	
	
	@ApiModelProperty(value="用户名", required = true)
	private String name;
	
	@ApiModelProperty(value="年龄", required = true)
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	
}
