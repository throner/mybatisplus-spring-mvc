/* 
* 2015-11-21 
* MySwaggerConfig.java 
* author:Zack Chan
*/ 
package com.swagger;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
// Loads the spring beans required by the framework
@ComponentScan(basePackages={"com.baomidou.springmvc.controller"})
public class MySwaggerConfig
{

    public Docket swaggerSpringMvcPlugin(){
        System.out.println("swaggerSpringMvcPlugin");
        return new Docket(DocumentationType.SWAGGER_2);
    }
}
