package com.cache;

import java.io.Serializable;

/**
 * Created by chenjiansheng on 2017-7-29.
 */
public class CacheObj implements Serializable {
    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
