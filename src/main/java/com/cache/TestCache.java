package com.cache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenjiansheng on 2017-7-29.
 */
@Component
public class TestCache {
//    #root.methodName 被执行方法的名称
//    #root.method.name 被执行的方法
//    #root.target 被执行的目标对象
//    #root.targetClass 被执行的目标对象的class
//    #root.args[0] 调用目标方法的时候目标方法里面的参数（可将参数列表类比成对象数组）的第一个
//    #root.caches[0].name 获取当前执行方法的缓存
    @Cacheable(value = "testCache", key = "#input")
    public String getString(String input){
        System.out.println("getString.......");
        return input+" value";
    }
    @CachePut(value = "testCache", key = "#input")
    public String setString(String input){
        System.out.println("setString:"+input);
        return input+" value";
    }
    @CacheEvict(value = "testCache", key = "#input")
    public String delString(String input){
        System.out.println("delString:"+input);
        return input;
    }

    @Cacheable(value = "testCacheObj", key = "#o.key")
    public CacheObj getCacheObj(CacheObj o){
        System.out.println("getCacheObj...............");
        CacheObj result = new CacheObj();
        result.setKey(o.getKey());
        result.setValue(o.getValue()+" value");
        return o;
    }

    @CachePut(value = "testCacheObj", key = "#o.key")
    public CacheObj setCacheObj(CacheObj o){
        System.out.println("setCacheObj...............");
        return o;
    }

    @CacheEvict(value = "testCacheObj", key = "#o.key")
    public CacheObj delCacheObj(CacheObj o){
        System.out.println("delCacheObj...............");
        return o;
    }

    // 缓存列表，返回值就是缓存的内容
    @Cacheable(value = "testCacheObj", key = "#o.key")
    public List<CacheObj> getCacheObjList(CacheObj o){
        System.out.println("getCacheObjList...............");
        List<CacheObj> list = new ArrayList<>();
        list.add(o);
        return list;
    }
}
